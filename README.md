# PostgreSQL Docker Compile Image

This repo provides a [Dockerfile](./Dockerfile) that compiles PostgreSQL ([version 15.3](https://www.postgresql.org/ftp/source/v15.3/) by default)

Running
```
docker build . -t postgrescompile --build-arg PG_VERSION=15.3
docker run --rm -it -v "$(realpath target)":/target:Z postgrescompile
```
will compile binaries for x86_64, arm, arm64 linux architechtures respectively.  
*Note: The arm builds are compiled without readline and zlib.*

The final tarballs can be then found in `target`.
To allow PostgreSQL to run in any directory, the actual binaries are replaced by scripts which set the environment variable `LD_LIBRARY_PATH` and after that start the binary file.

# Pre-Compiled binaries
The directory `target` contains the compiled PostgreSQL software inside tarballs.

The PostgreSQL software and its source code is licensed under [this license](https://www.postgresql.org/about/licence/).

```
Portions Copyright © 1996-2023, The PostgreSQL Global Development Group
Portions Copyright © 1994, The Regents of the University of California
```

The tarballs also include shell scripts (see above); these are licensed differently, see below. 

# License
This project, except the Postgres software in the tarballs (see above) is licensed under the [MIT license](./LICENSE).
