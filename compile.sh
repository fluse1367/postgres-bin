#!/bin/bash


for pair in \
x86_64-linux, \
arm-linux,arm-linux-gnueabihf-gcc \
arm64-linux,aarch64-linux-gnu-gcc \
; do 
	
	arch="${pair%,*}"
	compiler="${pair#*,}"
	
	# compile
	export CC="$compiler"
	cd /compile
	
	# exclude readline and zlib from CC builds
	if [ -z "$compiler" ]; then
		./configure --prefix /out --host "$arch"
	else
		./configure --prefix /out --host "$arch" --without-readline --without-zlib
	fi
	
	make clean
	make
	make install
	unset CC

	# replace all binary files with scripts that set the library path and then exectue the actual binary
	IFS=$'\n'; for f in $(find /out/bin/ -type f); do
		
		binfile="${f}.bin"
		mv "$f" "$binfile"

		cat <<EOF | sed "s/~BINNAME~/$(basename "$binfile")/" > "$f"
dir="\$(dirname "\$(readlink -f "\$0")")"
export LD_LIBRARY_PATH="\$(readlink -f "\$dir/../lib")"
"\$dir/~BINNAME~" \${@:1}
EOF
		chmod +x "$f"

	done
	
	
	# tar the binaries
	ftar=/target/postgresql-$PG_VER-$arch-bin.tar.gz
	cd /out
	cp /compile/COPYRIGHT .
	tar -czvf $ftar bin lib share COPYRIGHT
	cd -
	
	# calc checksum
	echo "$(md5sum $ftar | cut -d ' ' -f 1)" > "${ftar}.md5"
	
	# clean
	rm -rf /out

done

