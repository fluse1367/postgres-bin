FROM ubuntu

# install deps
RUN apt-get -y update
RUN apt-get -y install build-essential libreadline-dev zlib1g-dev flex bison libxml2-dev libxslt-dev libssl-dev libxml2-utils xsltproc ccache
RUN apt-get -y install wget
RUN apt-get -y install crossbuild-essential-arm64 crossbuild-essential-armhf

WORKDIR /compile
VOLUME /target
ARG PG_VERSION="15.3"
ENV PG_VER=${PG_VERSION}

# get postgres
RUN wget "https://ftp.postgresql.org/pub/source/v${PG_VER}/postgresql-${PG_VER}.tar.gz" -O /postgres.tar.gz
RUN tar -xzvf /postgres.tar.gz --strip-components 1

# compile script
COPY compile.sh /compile.sh

# entry: compile
ENTRYPOINT bash /compile.sh
